#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//struct para crear los puntos
struct Point {
    int x, y;
};

//struct que calcula la distancia entre los dos puntos que recibe como parametro
double getDistancia(struct Point a, struct Point b)
{
    double distancia;
    distancia = sqrt((a.x - b.x) * (a.x - b.x) + (a.y-b.y) *(a.y-b.y));
    return distancia;
}

//struct que calcula la pendiente de los puntos que recibe como parametro
double getpendiente(struct Point a, struct Point b)
{
    double pendiente;
  pendiente = (b.y - a.y) / (b.x - a.x);
    return pendiente;
}

int main(void)
{  
    struct Point a, b;
    printf("Ingrese la cordenada del punto a: ");
    scanf("%d %d", &a.x, &a.y);
    printf("Ingrese la cordenada del punto b: ");
    scanf("%d %d", &b.x, &b.y);
   
    
    printf("la distancia entre los puntos a y b: %lf\n", getDistancia(a, b));
    //impresion que muestra la forma de la ecuacion
   printf("La ecuacion de la recta es: Y - %i = %lf ( X - %i) \n" , a.y,getpendiente(a,b),a.x);
    
   
    
    
   
}
